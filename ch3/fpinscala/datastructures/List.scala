package fpinscala.datastructures

sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head:A, tail: List[A]) extends List[A]

object List {
    def sum[A](ints: List[Int]): Int = ints match {
        case Nil => 0
        case Cons(a, as) => a + sum(as)
    }

    def product[A](ds: List[Double]): Double = ds match {
        case Nil => 1.0
        case Cons(0.0, _) => 0
        case Cons(a, as) => a * product(as)
    }

    def sum1[A](ints: List[Int]): Int = ints match {
        case Nil => 0
        case Cons(a, as) => a + sum1(as)
    }

    def product1[A](ds: List[Double]): Double = ds match {
        case Nil => 1.0
        case Cons(0,_) => 0.0
        case Cons(a, as) => a * product1(as)
    }

    def foldRight[A,B](as: List[A], z:B)(f:(A,B) => B):B = {
        case Nil => z
        case Cons(x, xs) => ??? 
    }

    def apply[A](as: A*): List[A] = {
        if (as.isEmpty) Nil
        else Cons(as.head, apply(as.tail:_*))
    }

    def tail[A](as: List[A]): List[A] = as match {
        case Nil => Nil
        case Cons(a, as) => as
    }

    def setHead[A](a:A, as: List[A]): List[A] = as match {
        case Nil => Cons(a, Nil)
        case Cons(b, bs) => Cons(a, bs) 
    }

    def drop[A](n: Int, as: List[A]): List[A] = {
        def loop[A](n: Int, l: List[A]): List[A] = l match {
            case Nil => Nil
            case Cons(b, bs) => {
                if (n-1 > 0) loop(n - 1, bs)
                else bs
            }
        }

        loop(n, as)
    }

    def dropWhile[A](as: List[A])(f:A => Boolean): List[A] = {
        def loop[A](ls:List[A], f:A => Boolean): List[A] = ls match {
            case Nil => Nil
            case Cons(b, bs) => {
                if(f(b)) loop(bs, f)
                else ls
            }
        }
        loop(as, f)
    }

    def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
        case Nil => a2
        case Cons(a, as) => Cons(a, append(as, a2))
    }

    def init[A](l:List[A]): List[A] = {
        def loop(l:List[A]): List[A] = {
            l match {
                case Nil => Nil
                case Cons(b, Nil) => Nil
                case Cons(b, Cons(b1, Nil)) => Cons(b, Nil)
                case Cons(b, bs) => Cons(b,loop(bs))
            }
        }
        loop(l)
    }
}



