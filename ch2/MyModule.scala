
object MyModule {
    def abs(n: Int): Int = 
        if (n < 0) -n
        else n

    def factorial(n: Int):Int = {
        @annotation.tailrec
        def go(acc: Int, n: Int): Int = {
            if (n <= 0) acc
            else go(acc * n, n - 1)
        }

        go(1,n)
    }

    def fib(n:Int): Int = {
        def go(n: Int, last: Int, secondLast: Int): Int = {
            if (n == 0) last
            else go(n - 1, last + secondLast, last)
        }

        go(n, 0, 1)
    }

    private def formatFib(n: Int) = {
        val msg = "Fibonacci no: %d is %d"
        msg.format(n, fib(n))
    }
        

    private def formatAbs(n: Int) = {
        val msg = "Absolute value of %d is %d"
        msg.format(n, abs(n))
    }

    private def formatFactorial(n: Int): String = {
        val msg = "Factorial of %d is %d"
        msg.format(n, factorial(n))
    }

    private def formatValue(name: String, n: Int, f: Int => Int) = {
        val msg = "The %s of %d is %d"
        msg.format(name, n, f(n))
    }

    def main(args: Array[String]): Unit = {
        println(formatAbs(-42))

        println(formatFactorial(10))

        println(formatFib(26))

        println(formatValue("factorial", 10, factorial))
    }
}
