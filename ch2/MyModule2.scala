
object MyModule2 {

    def findFirst[A](as: Array[A], p: A => Boolean ): Int = {
        def loop(index: Int): Int = {
            if (index == as.length) -1
            else if (p(as(index))) return index
            else loop (index + 1)
        }
        loop(0)
    }

    def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean = {
        def loop(index: Int): Boolean = {
            if (index == as.length - 1) true
            else if (ordered(as(index),as(index + 1))) loop(index + 1)
            else false
        }

        loop(0)
    }

    def partial1[A,B,C](a:A, f:(A,B)=>C): B => C  = {
        b:B => f(a, b)
    }

    def curry[A,B,C](f: (A,B) => C): A => ( B => C) = {
        a:A => b:B => f(a,b)
    }

    def uncurry[A,B,C](f: A => B => C): (A,B) => C = {
        (a:A, b:B) => f(a)(b)
    }

    def compose[A,B,C](f: B => C, g: A => B): A => C = {
        a:A => f(g(a))
    }


    def main (args: Array[String]): Unit = {
        println("3 found at index : " + findFirst(Array(1,2,3,4,5), (x: Int)=> x == 3))
        println("hello3 found at index: " + findFirst(Array("Hello1","Hello2", "hello3"), (s:String) => s.equalsIgnoreCase("hello3")))
        println("1,2,3,4,5,10,11 isSorted " + isSorted(Array(1,2,3,4,5,10,11), (a:Int, b:Int) => a <= b) )
        println("1,7,3,4,5,10,11 isSorted " + isSorted(Array(1,7,3,4,5,10,11), (a:Int, b:Int) => a <= b) )
    }
}
