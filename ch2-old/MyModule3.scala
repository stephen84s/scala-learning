object MyModule3 {
    def isSorted[A](as: Array[A],f:(A,A)=>Boolean): Boolean = {
        @annotation.tailrec
        def go(count: Int):Boolean = {
	    if (count == as.length-1) true
	    else if (!f(as(count),as(count+1))) false
	    else go(count + 1)
	}
        go(0)
    }

    def main(args: Array[String]): Unit = {
        println("Is Sorted : " + isSorted[Int](Array(1, 2, 3, 4, 5),(x,y) => x<=y))
	println("Is Sorted : " + isSorted[Int](Array(1, 2, 5, 3),(x,y) => x<=y))
    }


}
