object MyModule {
    def abs(n: Int): Int = {
        if (n < 0) -n
        else n
    }


    def factorial(n: Int): Int = {
        def doFactorial(m: Int, acc: Int): Int = {
            if (m < 2) acc 
            else doFactorial(m - 1, acc * m)
        }
        doFactorial(n,1)
    }
    

    private def formatResult(s: String, x: Int, f: Int=>Int): String = {
        var msg = "The %s of %d is %d"
        msg.format(s, x, factorial(x))
    } 

    def main(args: Array[String]): Unit = {
        println(formatResult("absolute value",10000,abs))
        println(formatResult("absolute value", -192, abs))

        println(formatResult("factorial", 7, factorial))
        println(formatResult("factorial", 5, factorial))

    }
}
