object MyModule2 {
    def fibonacci(n: Int): Int = {
        @annotation.tailrec
        def go(x:Int, y:Int, count: Int): Int = {
            if (n <= 0) 0
            else if (count == n) x
            else go(y, x+y, count + 1)
        }
        go(0,1,1)
    }


    private def formatFib(n: Integer): String = {
        val msg = "No: %d in the fibonacci sequence is %d"
        msg.format(n, fibonacci(n))
    }

    def main(args: Array[String]): Unit  = {
        println(formatFib(10))
    }
}
